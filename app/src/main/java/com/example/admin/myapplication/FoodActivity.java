package com.example.admin.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class FoodActivity extends AppCompatActivity {

    ListView myGrid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food);

        myGrid = findViewById(R.id.myGrid);

        List<FoodModel> foods = new ArrayList<>();

        FoodModel ghormezabzi = new FoodModel("ghormzesabzi", 20000, "https://www.thedeliciouscrescent.com/wp-content/uploads/2016/11/Persian-Fresh-Herb-Stew-with-Meat-and-Kidney-Beans-2.jpg", "Meat , rice , vegetable, Beans");
        FoodModel gheyme = new FoodModel("gheyme", 18000, "https://s3-media1.fl.yelpcdn.com/bphoto/y0L5oqMvo1UY-qGwn_QhUg/o.jpg", "Meat , potato,Lentils");
        FoodModel sabzipolo = new FoodModel("sabzipolo", 26000, "http://www.thepersianpot.com/wp-content/uploads/IMG-20150312-WA0018.jpg", "Fish , rice , vegetable");
        FoodModel tahchin = new FoodModel("tahchin", 28000, "https://3.bp.blogspot.com/-9QmxSsK5cl0/TubcsPsRg2I/AAAAAAAACK8/eF-GmhVaFaU/s1600/Tah-Chin-TS.jpg", "Chicken , rice , Egg , Barberry");
        FoodModel zereshkpolo = new FoodModel("zereshkpolo", 27000, "http://2.bp.blogspot.com/_uk9R8SAqWsk/TGdVrMH8VzI/AAAAAAAABNc/NilZYRP4abU/s1600/5.jpg", "Meat , rice , vegetable,Barberry");

        foods.add(ghormezabzi);
        foods.add(gheyme);
        foods.add(sabzipolo);
        foods.add(tahchin);
        foods.add(zereshkpolo);

        FoodListAdapter adapter = new FoodListAdapter(this, foods);
        myGrid.setAdapter(adapter);


    }
}
