package com.example.admin.myapplication;

/**
 * Created by Admin on 2/4/2018.
 */

public class FoodModel {

    String name;
    int price;
    String image;
    String ingredient;


    public FoodModel(String name, int price, String image, String ingredient) {
        this.name = name;
        this.price = price;
        this.image = image;
        this.ingredient = ingredient;
    }

    public FoodModel(String ghormzesabzi, int i, String s) {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIngredient() {
        return ingredient;
    }

    public void setIngredient(String ingredient) {
        this.ingredient = ingredient;
    }
}
