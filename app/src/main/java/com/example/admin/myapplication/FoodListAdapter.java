package com.example.admin.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.security.BasicPermission;
import java.util.List;

/**
 * Created by Admin on 2/4/2018.
 */

public class FoodListAdapter extends BaseAdapter {

    Context mContext;
    List<FoodModel> foods;

    public FoodListAdapter(Context mContext, List<FoodModel> foods) {
        this.mContext = mContext;
        this.foods = foods;
    }

    @Override
    public int getCount() {
        return foods.size();
    }

    @Override
    public Object getItem(int i) {
        return foods.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View viewvalue = LayoutInflater.from(mContext).inflate(R.layout.foodslist, null);

        ImageView foodImage = viewvalue.findViewById(R.id.foodImage);
        TextView foodName = viewvalue.findViewById(R.id.foodName);
        TextView foodPrice = viewvalue.findViewById(R.id.foodPrice);
        TextView foodIngredient = viewvalue.findViewById(R.id.foodIngredient);


        foodName.setText(foods.get(i).getName());
        foodPrice.setText(foods.get(i).getPrice() + "");
        foodIngredient.setText(foods.get(i).getIngredient());
        Picasso.with(mContext).load(foods.get(i).getImage()).into(foodImage);


        return viewvalue;
    }
}
